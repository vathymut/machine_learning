function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%

% Add ones to the X data matrix to create Activation values of first layer
% size(Act_1) = [5000 401]
Act_1 = [ones(m, 1) X];

% Activation values of hidden (2nd) layer; Note that expression: X*(Theta1') is z_2
% Theta1 = [25 401];
Act_2 = sigmoid( Act_1*(Theta1') ); 	

% Add ones to the Act_2 data matrix
% size(Act_2) = [5000 26]; 26 = (25+1)
Act_2 = [ones(m, 1) Act_2];

% Predict chance of belonging to a class for the output layer (Act_3) ;
% Theta2 = [10 26]; size(Act_3) = [5000 10]
Act_3 = sigmoid( Act_2*(Theta2') );

% Set up variables in cost function;
for x = 1:num_labels
	% cost if class == 1 i.e. presence of class
	pr_pre = -(y == x).*log( Act_3(:, x) );
	% cost if class == 0 i.e. absence of class
	pr_abs = (ones(m, 1) - ( y == x ) ).*log( ones(m, 1) - Act_3(:, x) ); 
	% Cost function for class x
	Jclass(x) = sum( pr_pre - pr_abs )/m;
end

% Remove bias (intercept) coefficients
par_1 = Theta1(:, 2:end);
par_2 = Theta2(:, 2:end);
% Take the square of each remaining coefficient
sqpar1 = par_1.^2;
sqpar2 = par_2.^2;

% Regularized cost function for all class:
J = sum( Jclass ) + (lambda/(2*m))*( sum(sqpar1(:)) + sum(sqpar2(:)) );

% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%

% Pre-allocate memories for Deltas (error term);
% size(Act_3) = [5000 10]
deltas_3 = zeros(size(Act_3)); % error term of output layer;

% Create matrix of output value in binary format;
mat_y = zeros(size(Act_3));
for i = 1:num_labels
	mat_y(:, i) = (y == i);
end

% Compute error terms for final (output) layer i.e. deltas_3;
deltas_3 = Act_3 - mat_y;

% Pre-allocate memories for Deltas (error term);
% size(Act_2) = [5000 26];
deltas_2 = zeros( size(Act_2) ); % error term of hidden layer;

Z_2 = [ones(m, 1) Act_1*(Theta1')];
deltas_2 = deltas_3*(Theta2) .* sigmoidGradient( Z_2 ); 

% Denote Capital deltas by DELT;
% Preallocate memory for DELT_2
DELT_2 = zeros( size(Theta2) );
DELT_2 = DELT_2 + ( (deltas_3')*(Act_2) );

% Preallocate memory for DELT_2
DELT_1 = zeros( size(Theta1) );

% Adjust deltas_2 to remove first column:
% Why - the bias unit does not propagate in the preceding layer;
adjdeltas_2 = deltas_2(:, 2:end);
DELT_1 = DELT_1 + ( (adjdeltas_2')*(Act_1) );

% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

% Vectorised implementation for gradient:
% Preallocate memory
Theta1_grad = zeros( size(Theta1) );
Theta2_grad = zeros( size(Theta2) );
% Unregularized coefficients
Theta1_grad(:,1) = DELT_1(:,1)*(1/m);
Theta2_grad(:,1) = DELT_2(:,1)*(1/m);
% Regularized coefficients
Theta1_grad(:,2:end) = DELT_1(:,2:end)*(1/m) + (lambda/m)*Theta1(:,2:end);
Theta2_grad(:,2:end) = DELT_2(:,2:end)*(1/m) + (lambda/m)*Theta2(:,2:end);

% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];

end;
