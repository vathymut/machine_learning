function p = predict(Theta1, Theta2, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(Theta2, 1);

% You need to return the following variables correctly 
p = zeros(size(X, 1), 1);

% Add ones to the X data matrix
X = [ones(m, 1) X];

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the following code to make predictions using
%               your learned neural network. You should set p to a 
%               vector containing labels between 1 to num_labels.
%
% Hint: The max function might come in useful. In particular, the max
%       function can also return the index of the max element, for more
%       information see 'help max'. If your examples are in rows, then, you
%       can use max(A, [], 2) to obtain the max for each row.
%

% Activation values of hidden (2nd) layer
% Note that expression: X*(Theta1') is z_2
Act_2 = sigmoid( X*(Theta1') ); 	

% Add ones to the Act_2 data matrix
Act_2 = [ones(m, 1) Act_2];

% Predict chance of belonging to a class for the output layer (out_layer) ;
out_layer = sigmoid( Act_2*(Theta2') );

% Return maximum value (i.e. val) and index no. (i.e. p aka the row number);
[val, p] = max(out_layer, [], 2);
% =========================================================================


end
