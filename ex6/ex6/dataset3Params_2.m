function [C, sigma] = dataset3Params(X, y, Xval, yval)
%EX6PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = EX6PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
%C = 1;
%sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%
		
C_test = [0.1 0.3 1 3];
sigma_test = [0.1 0.3 1 3];

%Create preallocated matrix of results for all combinations of C and sigma;
error = zeros(length(C_test), length(sigma_test)); 

for i = 1:length(C_test)
	for ii = 1:length(sigma_test)
		model= svmTrain(X, y, C_test(i), @(x1, x2) gaussianKernel(x1, x2, sigma_test(ii))); 
		predictions = svmPredict(model, Xval);
		error(i,ii) = mean(double(predictions ~= yval));
	end
end

[aj,bj]=min(error); [cj,dj]=min(aj); %bj(dj) will be the row where the minimum error is, while dj will be its column
C=C_test(bj(dj)); sigma=sigma_test(dj); %so following the way my matrix of errors was built, Ci(bj(dj)) will be my best C value, while sigmai(dj) will be my best sigma value.

% =========================================================================

end
