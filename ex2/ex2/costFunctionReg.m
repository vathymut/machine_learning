function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));
pr_abs = zeros(m);
pr_pre = zeros(m);

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

% Vectorised solution for Cost Function:
% sigmoid predictions
sig_pr = sigmoid( X*(theta) ); 

% cost if class == 0 i.e. absence of class
pr_abs = (ones(size(y)) - y).*log(ones(size(y)) - sig_pr); 

% cost if class == 1 i.e. presence of class
pr_pre = -y.*log(sig_pr);

% mean residuals + regularized objective
J = sum( pr_pre - pr_abs )/m + (lambda/(2*m))*sum( theta(2:end).^2 ); 

% Vectorised implementation for gradient:
grad = ( (sig_pr - y)'*X )/m;
grad(2:end) = grad(2:end) + (lambda/m)*theta(2:end)';

% =============================================================

end
