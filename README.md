# README

Folders of code written in Octave/MATLAB for the [Machine Learing](http://ml-class.org) course on Coursera.

## Exercises
See folders `ex1-ex8`. The .pdf files - with the project instructions - along with .mat and .txt files - containing the data needed to complete the projects - are omitted. 

## Statement of Accomplishment
For the statement of accomplishment, see the file `Coursera ml 2014.pdf`


